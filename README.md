# Stack de desenvolvimento

- Editor: VS Code
- Extração de assets: Photoshop CC
- Ferramenta de build: Webpack
- Gerenciador de pacotes: NPM/Yarn
- Lib/Framework JS: React.js v16.6.0
- Linguagem de marcação: JSX/HTML5
- Pré-processador CSS: SASS/SCSS
- Servidor: Node.js v9.11.1

# Tecnologias e conceitos

**API**

- Mock importado como JSON

**CSS**

- Animações utilizando keyframes
- CSS Grid para estruturas externas
- CSS Flexbox para estruturas internas
- Metodologia: BEM (Block Element Modifier)
- Mobile-first
- Não foi utilizado nenhum Framework CSS
- Propriedades CSS em ordem alfabética
- SASS/SCSS 
	- Compilação de .scss via dependência node-sass (sem necessidade de Chokidar, Ruby Devkit, Gulp, etc.)
	- Mixins para breakpoints de media queries
	- Variáveis para cores
- Uso de object-fit para evitar quebras de layout em imagens com tamanhos diferentes retornadas da API

**HTML**

- SEO através de metatags e semântica
- Acessibilidade através de semântica e atributos que auxiliam tags

**JAVASCRIPT**

- Alteração de layout entre grid e lista via state
- Comportamento de menu e busca em mobile tratados via state
- Map em arrays para geração da lista de produtos com base na API

# Instruções para rodar o projeto

- `npm install` para instalar as dependências necessárias
- `npm start` para rodar o projeto em servidor local