import React, { Component } from 'react'
import '../Breadcrumb/breadcrumb.scss'

export default class Breadcrumb extends Component {

    render() {
        return (
            <ul className="breadcrumb container">
                <li>Página inicial</li>
                <li>Sapatos</li>
            </ul>
        )
    }

}