import React, { Component } from 'react'
import './global.scss'

import Breadcrumb from './components/Breadcrumb/Breadcrumb'
import Footer from './components/Footer/Footer'
import Header from './components/Header/Header'
import Showcase from './components/Showcase/Showcase'
import Sidebar from './components/Sidebar/Sidebar'

export default class App extends Component {

	render() {
		return (
			<div>

				<Header />

				<Breadcrumb />

				<main className="grid-container container">
					<div className="grid-item">
						<Sidebar />
					</div>
					<div className="grid-item">
						<Showcase />
					</div>
				</main>

				<Footer />

			</div>
		)
	}
}
