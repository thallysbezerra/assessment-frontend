import React, { Component } from 'react'
import '../Header/header.scss'

export default class Header extends Component {

    constructor(props) {
        super(props);

        this.state = { 
            showMenu: false,
            showSearch: false
        }

        this.showMenu = this.showMenu.bind(this);
        this.hideMenu = this.hideMenu.bind(this);
        this.showSearch = this.showSearch.bind(this);
    }

    showMenu() { this.setState({ showMenu: true }) }
    hideMenu() { this.setState({ showMenu: false }) }
    showSearch() { this.setState({ showSearch: true }) }

    render() {
        return (
            <header className="header">

                <div className="header__loginArea">
                    <div className="container">
                        <p><a href="#">Acesse sua Conta</a> ou <a href="#">Cadastre-se</a></p>
                    </div>
                </div>

                <div>

                    <div className="container">

                        <div className="menu">
                        
                            <i className="icon-bars" onClick={this.showMenu}></i>
                            <a href="#" className="logo"><img src="/img/logo-webjump.png" alt=""/></a>
                            <i className={this.state.showSearch == true ? 'icon-search hide' : 'icon-search'} onClick={this.showSearch}></i>

                        </div>

                        <div className={this.state.showSearch == true ? 'searchMenu show' : 'searchMenu'}>
                            <input type="text" className="inputSearch"/>
                            <button className="buttonSearch">Buscar</button>
                        </div>

                    </div>

                    <nav className={this.state.showMenu == true ? 'show' : ''}>
                        <i className="icon-close" onClick={this.hideMenu}></i>
                        <ul className="container">
                            <li><a href="#">Página inicial</a></li>
                            <li><a href="#">Camisetas</a></li>
                            <li><a href="#">Calças</a></li>
                            <li><a href="#">Sapatos</a></li>
                            <li><a href="#">Contato</a></li>
                        </ul>
                    </nav>

                </div>

            </header>
        )
    }

}