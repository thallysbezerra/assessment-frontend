import React, { Component } from 'react'
import '../Sidebar/sidebar.scss'

export default class Breadcrumb extends Component {

    render() {
        return (
            <div className="sidebar">

                <header className="sidebar__title">Filtre por</header>
                
                <div className="filterGroup">
                    <div className="filterGroup__title">Categorias</div>
                    <ul>
                        <li><a href="#">Roupas</a></li>
                        <li><a href="#">Sapatos</a></li>
                        <li><a href="#">Acessórios</a></li>
                    </ul>
                </div>

                <div className="filterGroup filterGroup--colors">
                    <div className="filterGroup__title">Cores</div>
                    <ul>
                        <li><a href="#" data-info="vermelho">Vermelho</a></li>
                        <li><a href="#" data-info="laranja">Laranja</a></li>
                        <li><a href="#" data-info="azul">Azul</a></li>
                    </ul>
                </div>

                <div className="filterGroup">
                    <div className="filterGroup__title">Tipo</div>
                    <ul>
                        <li><a href="#">Corrida</a></li>
                        <li><a href="#">Caminhada</a></li>
                        <li><a href="#">Casual</a></li>
                        <li><a href="#">Social</a></li>
                    </ul>
                </div>

            </div>
        )
    }

}