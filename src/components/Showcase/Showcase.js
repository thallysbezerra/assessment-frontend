import React, { Component } from 'react'
import '../Showcase/showcase.scss'

import dataList from '../../api/V1/categories/list.json'
import dataPants from '../../api/V1/categories/pants.json'
import dataShoes from '../../api/V1/categories/shoes.json'
import dataTshirts from '../../api/V1/categories/tshirts.json'

export default class Showcase extends Component {

    constructor(props) {
        super(props);

        this.state = { 
            list: false
        }

        this.showGrid = this.showGrid.bind(this);
        this.showList = this.showList.bind(this);
    }

    showGrid() { this.setState({ list: false }) }
    showList() { this.setState({ list: true }) }

    listItems = (props) => {

        const list = dataShoes.items.map((item, index) =>

            <li key={index}>
                <a href="#" className="products__box"><img src={item.image} alt="" /></a>
                <div>
                    <p className="products__name">{item.name}</p>
                    <p className="products__price">R$ {item.price.toFixed(2).replace('.',',')}</p>
                    <a href="#" className="products__button">Comprar</a>
                </div>
            </li>    

        );
        return list;      

    }
    

    render() {
        return (
            <div className="showcase">

                <h2>Sapatos</h2>

                <div className="tools">
                    <div className="tools__grid">
                        <i className={this.state.list == false ? 'icon-th active' : 'icon-th'} onClick={this.showGrid}></i>
                        <i className={this.state.list == true ? 'icon-th-list active' : 'icon-th-list'} onClick={this.showList}></i>
                    </div>
                    <div className="tools__order">
                        <label htmlFor="order">Ordenar por</label>
                        <select name="order" id="order">
                            <option value="barato">Mais barato</option>
                            <option value="caro">Mais caro</option>
                        </select>
                    </div>
                </div>

                <div className="products">

                    <ul className={this.state.list == true ? 'list' : ''}>
                        {this.listItems()}                                                                                     
                    </ul>
                    
                </div>

            </div>
        )
    }

}